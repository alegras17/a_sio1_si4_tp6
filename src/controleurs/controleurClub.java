
package controleurs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;


public class controleurClub {

        int salaireBrut;
        int nbEnfants;        
        int anciennete;        
        int primeBase;
        int primeEnfants;
        int primeAnciennete;
        int primeVac;        
        
public void calculer() {

    

}
        
        
        
        //<editor-fold defaultstate="collapsed" desc="code généré">
        public static final String PROP_SALAIREBRUT = "salaireBrut";
        
        public int getSalaireBrut() {
            return salaireBrut;
        }
        
        public void setSalaireBrut(int salaireBrut) {
            int oldSalaireBrut = this.salaireBrut;
            this.salaireBrut = salaireBrut;
            propertyChangeSupport.firePropertyChange(PROP_SALAIREBRUT, oldSalaireBrut, salaireBrut);
        }
        private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
        
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
        
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
        
        
        public static final String PROP_NBENFANTS = "nbEnfants";
        
        public int getNbEnfants() {
            return nbEnfants;
        }
        
        public void setNbEnfants(int nbEnfants) {
            int oldNbEnfants = this.nbEnfants;
            this.nbEnfants = nbEnfants;
            propertyChangeSupport.firePropertyChange(PROP_NBENFANTS, oldNbEnfants, nbEnfants);
        }
        
        
        public static final String PROP_ANCIENNETE = "anciennete";
        
        public int getAnciennete() {
            return anciennete;
        }
        
        public void setAnciennete(int anciennete) {
            int oldAnciennete = this.anciennete;
            this.anciennete = anciennete;
            propertyChangeSupport.firePropertyChange(PROP_ANCIENNETE, oldAnciennete, anciennete);
        }
        
        
        public static final String PROP_PRIMEBASE = "primeBase";
        
        public int getPrimeBase() {
            return primeBase;
        }
        
        public void setPrimeBase(int primeBase) {
            int oldPrimeBase = this.primeBase;
            this.primeBase = primeBase;
            propertyChangeSupport.firePropertyChange(PROP_PRIMEBASE, oldPrimeBase, primeBase);
        }
        
        
        public static final String PROP_PRIMEENFANTS = "primeEnfants";
        
        public int getPrimeEnfants() {
            return primeEnfants;
        }
        
        public void setPrimeEnfants(int primeEnfants) {
            int oldPrimeEnfants = this.primeEnfants;
            this.primeEnfants = primeEnfants;
            propertyChangeSupport.firePropertyChange(PROP_PRIMEENFANTS, oldPrimeEnfants, primeEnfants);
        }
        
        
        public static final String PROP_PRIMEANCIENNETE = "primeAnciennete";
        
        public int getPrimeAnciennete() {
            return primeAnciennete;
        }
        
        public void setPrimeAnciennete(int primeAnciennete) {
            int oldPrimeAnciennete = this.primeAnciennete;
            this.primeAnciennete = primeAnciennete;
            propertyChangeSupport.firePropertyChange(PROP_PRIMEANCIENNETE, oldPrimeAnciennete, primeAnciennete);
        }
        
        
        public static final String PROP_PRIMEVAC = "primeVac";
        
        public int getPrimeVac() {
            return primeVac;
        }
        
        public void setPrimeVac(int primeVac) {
            int oldPrimeVac = this.primeVac;
            this.primeVac = primeVac;
            propertyChangeSupport.firePropertyChange(PROP_PRIMEVAC, oldPrimeVac, primeVac);
        }
        //</editor-fold>

}
